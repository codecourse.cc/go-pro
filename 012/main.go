package main

import "fmt"

// iota
const (
	// iota starts with 0
	_ = iota
	// iota increments by 1
	KB = 1 << (10 * iota)
	MB = 1 << (10 * iota)
	GB = 1 << (10 * iota)
	TB = 1 << (10 * iota)
)

func main() {
	n := 4
	fmt.Printf("%d\t\t%b\n", n, n)

	// bit shifting
	m := n << 2
	fmt.Printf("%d\t\t%b\n", m, m)

	// iota usage | manual way

	kb := 1024
	mb := 1024 * kb
	gb := 1024 * mb
	tb := 1024 * gb

	fmt.Printf("%d\t\t%b\n", kb, kb)
	fmt.Printf("%d\t\t%b\n", mb, mb)
	fmt.Printf("%d\t%b\n", gb, gb)
	fmt.Printf("%d\t%b\n", tb, tb)

	// iota
	fmt.Printf("KB: %d\t\t%b\n", KB, KB)
	fmt.Printf("MB: %d\t\t%b\n", MB, MB)
	fmt.Printf("GB: %d\t\t%b\n", GB, GB)
	fmt.Printf("TB: %d\t%b\n", TB, TB)
}
