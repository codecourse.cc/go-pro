package main

import (
	"fmt"
	"math"
)

// define an interface
type geometry interface {
	area() float64
}

// define a struct
type rect struct {
	width, height float64
}

// define a struct
type circle struct {
	radius float64
}

// define a struct
type triangle struct {
	base   float64
	height float64
}

// rect has func called area
func (r rect) area() float64 {
	return r.width * r.height
}

// circle also has area func
func (c circle) area() float64 {
	return math.Pi * math.Pow(c.radius, 2)
}

// triangle also has area func
func (t triangle) area() float64 {
	return 0.5 * t.base * t.height
}

// measure func can use g interface
func measure(g geometry) {
	fmt.Println(g)
	fmt.Println(g.area())
	// can act based on type of underlying item.
	switch g.(type) {
	case rect:
		fmt.Printf("Rect has height of %v and width of %v so area is %v.\n", g.(rect).height, g.(rect).width, g.(rect).area())
	case circle:
		fmt.Printf("Cicle has radious of %v so area is %v.\n", g.(circle).radius, g.(circle).area())
	case triangle:
		fmt.Printf("Triangle has base of %v and height of %v so area is %v.\n", g.(triangle).base, g.(triangle).height, g.(triangle).area())
	}
}

func main() {
	r := rect{
		width:  4,
		height: 4,
	}

	c := circle{radius: 4}

	t := triangle{
		base:   20,
		height: 12,
	}

	measure(r)
	measure(c)
	measure(t)
}
