package main

import "fmt"

type device struct {
	name  string
	price string
}

type phone struct {
	device
	model string
}

// method | attach to phone struct
func (p phone) info() {
	fmt.Printf("%v is priced at %v and its model is %v.\n", p.name, p.price, p.model)
}

func main() {
	samsung := phone{
		device: device{
			name:  "Samsung Galaxy S9 Plus",
			price: "1000 USD",
		},
		model: "SM-G965",
	}
	fmt.Println(samsung)
	samsung.info()
}
