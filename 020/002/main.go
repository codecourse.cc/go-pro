package main

import "fmt"

func main() {
	fmt.Println(sum(12, 56, 156, 631, 545))
}

func sum(n ...int) int {
	total := 0

	for _, v := range n {
		total += v
	}

	return total
}
