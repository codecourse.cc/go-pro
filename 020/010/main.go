package main

import "fmt"

func main() {
	items := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}

	s := sum(items...)
	fmt.Println(s)

	sumOfEvens := even(sum, items...)
	fmt.Println(sumOfEvens)

	sumOfOdds := odd(sum, items...)
	fmt.Println(sumOfOdds)

}

func sum(x ...int) int {
	total := 0
	for _, v := range x {
		total += v
	}
	return total
}

func even(f func(x ...int) int, y ...int) int {
	var items []int
	for _, v := range y {
		if v%2 == 0 {
			items = append(items, v)
		}
	}

	return f(items...)
}

func odd(f func(x ...int) int, y ...int) int {
	var items []int
	for _, v := range y {
		if v%2 != 0 {
			items = append(items, v)
		}
	}

	return f(items...)
}
