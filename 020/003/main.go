package main

import "fmt"

func main() {
	// can also extract int slice onto variadic parameter
	n := []int{12, 56, 156, 631, 545}
	fmt.Println(sum(n...))
	// variadic parameter means 0 and more so we can add empty
	fmt.Println(sum())
}

func sum(n ...int) int {
	total := 0

	for _, v := range n {
		total += v
	}

	return total
}
