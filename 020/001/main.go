package main

import "fmt"

func main() {
	sayHi()
	greet("Azar")
	result := calc("add", 2, 2)
	fmt.Println(result)

	res, _ := command("cash", 50)
	fmt.Println(res)

	res, _ = command("credit", 800)
	fmt.Println(res)

	res, _ = command("credit", 2000)
	fmt.Println(res)

	res, _ = command("blah", 800)
	fmt.Println(res)
}

// basic func
func sayHi() {
	fmt.Println("Hi")
}

// func with argument
func greet(name string) {
	fmt.Printf("Hi %v\n", name)
}

// func with return
func calc(operation string, v1 float32, v2 float32) float32 {
	var result float32

	switch operation {
	case "add":
		result = v1 + v2
	}

	return result
}

// func with multi return
func command(cm string, price int) (string, bool) {
	switch cm {
	case "cash":
		if price > 10 && price < 100 {
			return "You can buy", true
		} else {
			return "No idea", false
		}
	case "credit":
		if price > 0 && price < 1000 {
			return "Can do", true
		} else {
			return "Maybe try calling us", true
		}
	}

	return "no", false
}
