package main

import "fmt"

func main() {
	// normal order of execution
	first()
	second()

	// defer commands the func to execute at the end of func body execution
	fmt.Println("Let's see...")
	defer first()
	second()
}

func first() {
	fmt.Println("I am first")
}

func second() {
	fmt.Println("I am second")
}
