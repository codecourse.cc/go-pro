# Functions

1. Syntax
2. Variadic Parameter
3. Extracting
4. Defer
5. Methods
6. Interfaces
7. Anonymous Functions
8. Func expression
9. Return a func
10. Callback
11. Closure
12. Recursion 