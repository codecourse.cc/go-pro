package main

import "fmt"

func main() {
	gr := greet()
	fmt.Println(gr)

	// func returns a func and then we execute
	f := number()
	r := f()
	fmt.Println(r)

	// create a func from returned func and use it for later
	double := maker(2)
	fmt.Println(double(2))
	fmt.Println(double(34))

	// triple func is used like this as well
	triple := maker(3)
	fmt.Println(triple(6))

	// custom creative naming
	tenify := maker(10)
	fmt.Println(tenify(40))

	iMultipleEverythingBy32 := maker(32)
	fmt.Println(iMultipleEverythingBy32(32))
}

// return a str
func greet() string {
	message := "Hi there"
	return message
}

func number() func() int {
	return func() int {
		return 1987
	}
}

func maker(x int) func(n int) int {
	return func(n int) int {
		return n * x
	}
}
