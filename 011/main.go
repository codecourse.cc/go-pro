package main

import "fmt"

func main() {
	// declare & assign
	const pi = 3.14159265359
	fmt.Println(pi)

	// cannot assign again
	//pi = 12.12

	// can group assign
	const (
		x = 10
		y = "I am"
		z = true
	)
	println(x, y, z)

	// can indicate type
	const (
		a int     = 32
		b string  = "We are"
		c float32 = 32.32
	)
	println(a, b, c)
}
