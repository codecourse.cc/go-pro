package main

import "fmt"

func main() {
	// general print to standard out.
	fmt.Print("A simple print\n")
	fmt.Printf("I can do much more with number %v.\n", 42)
	fmt.Println("I have my own line...")

	// this creates print to string which in turn can be assigned to a var
	line := fmt.Sprint("I wait my turn.\n")
	fmt.Print(line)
	// similar to Printf
	line = fmt.Sprintf("I can do much more with number %v later.\n", 42)
	fmt.Print(line)
	// similar to Println
	line = fmt.Sprintln("I have my own line... but I print later too")
	fmt.Print(line)
}
