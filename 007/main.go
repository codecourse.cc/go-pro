package main

import "fmt"

func main() {
	var age int = 18
	fmt.Printf("%v (age) is %T.\n", age, age)

	type entier int

	var âge entier = 2019
	fmt.Printf("%v (âge) is %T.\n", âge, âge)

	fmt.Println("âge's type prints as main.entier, but we can convert it back to int.")
	var ageUpdated = int(âge)
	fmt.Printf("%v (ageUpdated) is %T.\n", ageUpdated, ageUpdated)

}
