package main

import "fmt"

// define a struct
type person struct {
	first string
	last  string
	age   int
}

// define a struct that takes person in
type engineer struct {
	person
	position string
}

func main() {
	john := person{
		first: "John",
		last:  "Doe",
		age:   18,
	}

	mike := person{
		first: "Mike",
		last:  "Miller",
		age:   28,
	}

	fmt.Println(john, mike)

	// access part of struct
	fmt.Printf("Hello I am %v.\n", mike.first)

	// embedded struct
	kelly := engineer{
		person: person{
			first: "Kelly",
			last:  "Maker",
			age:   32,
		},
		position: "Field Engineer",
	}

	fmt.Println(kelly)

	// anonymous struct
	device := struct {
		name  string
		price int
	}{
		name:  "Device X",
		price: 122,
	}

	fmt.Println(device)
}
