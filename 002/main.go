package main

import "fmt"

// we can't do this here
//age := 32

func main() {
	// declare & assign at the same time
	name := "Azar"
	fmt.Println(name)
	// update
	name = "Azer"
	fmt.Println(name)
}
