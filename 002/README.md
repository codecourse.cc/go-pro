# Short Declaration Operator
It is used to make it all in one. We don't need to add var or define type, := does it all.

We can't create a **Short Declaration Operator** outside of a function body. We use **vars** instead if we want to declare a variable outside of a function body.