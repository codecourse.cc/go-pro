package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// the compiler assigns zero value which in bool's case is false
	var x bool
	fmt.Println(x)
	x = true
	fmt.Println(x)

	// seed the rand
	rand.Seed(time.Now().UnixNano())

	// comparision also produces bool result
	a := rand.Intn(100)
	b := rand.Intn(100)
	fmt.Printf("a is %v and b is %v\n", a, b)
	fmt.Println(a == b)
	fmt.Println(a != b)
	fmt.Println(a > b)
	fmt.Println(a >= b)
	fmt.Println(a < b)
	fmt.Println(a <= b)
}
