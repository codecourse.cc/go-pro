package main

import "fmt"

type person struct {
	firstName               string
	lastName                string
	favoriteIceCreamFlavors []string
}

func main() {
	personOne := person{
		firstName:               "Mike",
		lastName:                "Ko",
		favoriteIceCreamFlavors: []string{"vanilla", "chocolate"},
	}

	personTwo := person{
		firstName:               "James",
		lastName:                "Mona",
		favoriteIceCreamFlavors: []string{"chocolate", "cherry"},
	}

	people := map[string]person{
		"ko":   personOne,
		"mona": personTwo,
	}

	for _, p := range people {
		fmt.Println(p.firstName, p.lastName)
		for i, v := range p.favoriteIceCreamFlavors {
			fmt.Printf("\t%v, %v\n", i, v)
		}
	}
}
