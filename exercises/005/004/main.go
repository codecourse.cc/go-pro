package main

import "fmt"

func main() {
	course := struct {
		name  string
		price int
	}{
		name:  "Finance",
		price: 2000,
	}

	fmt.Println(course)
}
