package main

import "fmt"

func main() {
	items := []int{1, 2, 4, 5, 612, 45, 125}
	fmt.Println(foo(items...))

	fmt.Println(bar(items))
}

func foo(items ...int) int {
	total := 0
	for _, v := range items {
		total += v
	}

	return total
}

func bar(items []int) int {
	total := 0
	for _, v := range items {
		total += v
	}

	return total
}
