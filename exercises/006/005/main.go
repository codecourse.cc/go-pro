package main

import (
	"fmt"
	"math"
)

type circle struct {
	radius float64
}

type square struct {
	length float64
}

func (c circle) area() float64 {
	return math.Pow(c.radius, 2) * math.Pi
}

func (s square) area() float64 {
	return math.Pow(s.length, 2)
}

type shape interface {
	area() float64
}

func info(s shape) {
	fmt.Println(s.area())
}

func main() {
	c := circle{radius: 12.345}
	info(c)

	s := square{length: 40}
	info(s)
}
