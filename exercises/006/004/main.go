package main

import "fmt"

type person struct {
	first string
	last  string
	age   int
}

func (p person) speak() {
	fmt.Printf("I am %v and I am %v years old.", p.first, p.age)
}

func main() {
	azar := person{
		first: "Azar",
		last:  "Aliyev",
		age:   32,
	}

	azar.speak()
}
