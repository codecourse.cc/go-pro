package main

import "fmt"

func main() {
	defer why()
	fmt.Println("I am here first")
}

func why() {
	fmt.Println("Here is me!")
}
