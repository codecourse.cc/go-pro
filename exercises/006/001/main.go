package main

import "fmt"

func main() {
	n := foo()
	m, w := bar()
	fmt.Println(n)
	fmt.Println(m)
	fmt.Println(w)
}

func foo() int {
	return 10
}

func bar() (int, string) {
	return 20, "Go"
}
