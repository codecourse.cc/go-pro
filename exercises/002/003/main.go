package main

import "fmt"

const (
	// UNTYPED
	N = 10
	// TYPED
	Z int = 20
)

func main() {
	fmt.Println(N)
	fmt.Println(Z)
}
