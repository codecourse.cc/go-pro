package main

import "fmt"

func main() {
	e := 12 == 14
	ln := 23 <= 29
	gn := 56 >= 32
	ne := 44 != 22
	l := 34 < 79
	g := 12 > -12

	fmt.Printf("e is %v \n", e)
	fmt.Printf("ln is %v \n", ln)
	fmt.Printf("gn is %v \n", gn)
	fmt.Printf("ne is %v \n", ne)
	fmt.Printf("l is %v \n", l)
	fmt.Printf("g is %v \n", g)
}
