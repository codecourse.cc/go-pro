package main

import "fmt"

func main() {
	n := 16
	fmt.Printf("DECIMAL: %d\n", n)
	fmt.Printf("BINARY: %b\n", n)
	fmt.Printf("HEX: %#x\n", n)

	// shift 1 bit
	z := n << 1
	fmt.Printf("DECIMAL: %d\n", z)
	fmt.Printf("BINARY: %b\n", z)
	fmt.Printf("HEX: %#x\n", z)
}
