package main

import "fmt"

const (
	_ = 2019 + iota
	// no need to assign again since next year will take prev
	Y1
	Y2
	Y3
	Y4
)

func main() {
	fmt.Printf("Y1: %d\n", Y1)
	fmt.Printf("Y2: %d\n", Y2)
	fmt.Printf("Y3: %d\n", Y3)
	fmt.Printf("Y4: %d", Y4)
}
