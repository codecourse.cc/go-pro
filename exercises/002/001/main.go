package main

import "fmt"

func main() {
	n := 32
	fmt.Printf("DECIMAL: %d\n", n)
	fmt.Printf("BINARY: %b\n", n)
	fmt.Printf("HEX: %#x\n", n)
}
