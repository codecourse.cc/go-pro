# Level 2

- Use decimal, binary, and hex
- Use comparison operators
- Typed and Untyped constants
- Bit shifting
- Raw string literal (use of backticks)
- Use iota