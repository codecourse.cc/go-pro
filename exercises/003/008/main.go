package main

import "fmt"

func main() {

	name := "John"

	switch {
	case name == "Lili":
		fmt.Println("That is a girl's name.")
	case name == "John":
		fmt.Println("It is a boy's name.")
	default:
		fmt.Printf("I don't have any info on %v.\n", name)
	}
}
