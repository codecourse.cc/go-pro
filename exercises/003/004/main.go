package main

import (
	"fmt"
	"time"
)

func main() {
	currentYear := time.Now().Year()
	i := 1987
	for {
		if i > currentYear {
			break
		}
		fmt.Println(i)
		i++
	}
}
