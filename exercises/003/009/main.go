package main

import "fmt"

func main() {
	favSport := "football"
	switch favSport {
	case "basketball":
		fmt.Println("NBA is my favorite.")
	case "football":
		fmt.Println("I like it.")
	default:
		fmt.Printf("I need to check more about %v.\n", favSport)
	}
}
