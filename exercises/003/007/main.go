package main

import "fmt"

func main() {
	age := 28

	if age >= 18 {
		fmt.Println("You can drive!")
	} else if age >= 14 {
		fmt.Println("You need to wait a bit longer!!!")
	} else {
		fmt.Println("Sorry, you need to wait :(")
	}
}
