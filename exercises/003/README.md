# Level 3

- Use `for` loop in its common form
- Get rune code point of the uppercase alphabet
- Use `for` loop with condition only
- Use `for` loop with just curly
- Use modulus operator
- if statement
- if statement with else if and else
- Use switch statement with no switch expression
- Use switch statement with the switch expression
- Just a simple conditional checks