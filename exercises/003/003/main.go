package main

import (
	"fmt"
	"time"
)

func main() {
	currentYear := time.Now().Year()
	i := 1987
	for i <= currentYear {
		fmt.Println(i)
		i++
	}
}
