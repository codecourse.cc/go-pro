package main

import "fmt"

func main() {
	n := []int{42, 43, 44, 45, 46, 47, 48, 49, 50, 51}
	fmt.Println(n)

	fmt.Println(n[:5])
	fmt.Println(n[5:])
	fmt.Println(n[2:7])
	fmt.Println(n[1:6])
}
