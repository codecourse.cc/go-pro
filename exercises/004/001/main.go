package main

import "fmt"

func main() {
	n := [5]int{}
	fmt.Println(n)

	n[0] = 10
	n[1] = 11
	n[2] = 12
	n[3] = 13
	n[4] = 14

	fmt.Println(n)

	for i, v := range n {
		fmt.Println(i, v)
	}

	fmt.Printf("%T\n", n)
}
