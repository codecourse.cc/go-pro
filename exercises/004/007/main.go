package main

import "fmt"

func main() {
	james := []string{"James", "Bond", "Shaken, not stirred"}
	miss := []string{"Miss", "Moneypenny", "Helloooooo, James."}
	persons := [][]string{james, miss}

	fmt.Println(persons)

	for i, v := range persons {
		fmt.Println("record: ", i)
		for in, va := range v {
			fmt.Printf("\t index position: %v \t value: \t %v \n", in, va)
		}
	}
}
