package main

import "fmt"

func main() {
	m := map[string][]string{
		"bond_james":      []string{`Shaken, not stirred`, `Martinis`, `Women`},
		"moneypenny_miss": []string{`James Bond`, `Literature`, `Computer Science`},
		"no_dr":           []string{`Being evil`, `Ice cream`, `Sunsets`},
	}

	m["aliyev_azar"] = []string{`Learning`, "Ice Cream", "Music"}

	delete(m, "bond_james")

	for key, value := range m {
		fmt.Println(key)
		for i, v := range value {
			fmt.Printf("\tindex: %v\tvalue: %v\n", i, v)
		}
	}
}
