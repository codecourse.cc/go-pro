package main

import "fmt"

type ex int

var x ex
var y int

func main() {
	fmt.Println(x)
	fmt.Printf("x is type of %T\n", x)
	x = 42
	fmt.Println(x)

	// conversion
	y = int(x)
	fmt.Println(y)
	fmt.Printf("y is type of %T\n", y)
}
