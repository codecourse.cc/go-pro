- At the package level scope, using the "var" keyword, create a VARIABLE with the IDENTIFIER "y". The variable should be of the UNDERLYING TYPE of your custom TYPE "x"
- In func main
    - use CONVERSION to convert the TYPE of the VALUE stored in "x" to the UNDERLYING TYPE
        - then use the "=" operator to ASSIGN that value to "y"
        - print out the value stored in "y"
        - print out the type of "y"

_Using the code from the previous exercise_