package main

import "fmt"

// package level scope
var x int
var y string
var z bool

func main() {
	// the compiler assigns values automatically to declared vars
	// x is 0
	fmt.Printf("x is: %v\n", x)
	// y is empty string
	fmt.Printf("y is: %v\n", y)
	// z is false
	fmt.Printf("z is: %v\n", z)
}
