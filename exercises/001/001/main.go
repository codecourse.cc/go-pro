package main

import "fmt"

func main() {
	// declare & assign
	x := 42
	y := "James Bond"
	z := true

	// single
	fmt.Println(x, y, z)

	// multi
	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(z)
}
