package main

import "fmt"

// custom type | underlying type int
type ex int

// declare var with custom type
var x ex

func main() {
	fmt.Println(x)
	fmt.Printf("x is type of %T\n", x)
	x = 42
	fmt.Println(x)
}
