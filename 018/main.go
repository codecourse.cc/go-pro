package main

import "fmt"

func main() {
	// declare and create map
	m := map[string]int{
		"John": 30,
		"Mike": 12,
	}
	fmt.Println(m)

	// get single entry
	fmt.Println(m["John"])

	// a bit confusing to get 0 for none existence entry
	fmt.Println(m["Ho"])

	// we check whether entry in map
	v, ok := m["Ho"]
	fmt.Println(v, ok)

	// use in an if statement
	if v, ok := m["Ma"]; ok {
		fmt.Println("ITEM FOUND.", v)
	} else {
		fmt.Println("CAN'T FIND IT.", v)
	}

	// add new element to map
	m["Kaylo"] = 22
	fmt.Println(m)

	// loop through each item in a map
	for key, value := range m {
		fmt.Println(key, value)
	}

	// delete an item from map
	delete(m, "Kaylo")
	fmt.Println(m)
}
