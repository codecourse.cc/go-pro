package main

import (
	"fmt"
)

func main() {
	// declare & assign
	s := "I can do much more..."
	fmt.Println(s)
	fmt.Printf("%T\n", s)

	// convert string to byte slice
	bs := []byte(s)
	fmt.Println(bs)
	fmt.Printf("%T\n", bs)

	// UTF-8
	for i := 0; i < len(s); i++ {
		fmt.Printf("%#U ", s[i])
	}

	fmt.Println("\n")

	// hex
	for i := 0; i < len(s); i++ {
		fmt.Printf("%#x ", s[i])
	}

	fmt.Println("\n")

	// binary
	for i := 0; i < len(s); i++ {
		fmt.Printf("%b ", s[i])
	}
}
