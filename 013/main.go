package main

import "fmt"

func main() {
	// starter
	// for init; condition; post {}
	for i := 0; i < 100; i++ {
		fmt.Println(i)
	}

	// nested loop
	for n := 1; n <= 10; n++ {
		for m := 1; m <= 10; m++ {
			fmt.Printf("%v * %v = %v\n", n, m, n*m)
		}
		fmt.Println("-------------")
	}

	// single condition
	x := 1
	for x <= 3 {
		fmt.Println(x)
		x++
	}

	// break
	f := 1
	fmt.Println("-------------")
	for {
		if f > 4 {
			break
		}
		fmt.Printf("f is %v\n", f)
		f++
	}

	// continue | numbers divisible by 3 or by 5
	fmt.Println("-------------")
	for l := 1; l <= 100; l++ {
		if l%15 != 0 {
			continue
		}
		fmt.Printf("%v is divisible by 3 or by 5!\n", l)
	}
}
