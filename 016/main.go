package main

import "fmt"

func main() {
	// declare an array with 10 items (0 each)
	var n [10]int
	fmt.Println(n)
	// change the first item to have a value
	n[0] = 12
	fmt.Println(n)

	// can't add more than allocated space | this won't work
	// n[100] = 200

	// use 'len' to get length of array
	fmt.Println(len(n))
}
