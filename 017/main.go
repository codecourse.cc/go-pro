package main

import "fmt"

func main() {
	// composite literal
	n := []int{1, 2, 3, 4, 5}
	fmt.Println(n)

	// for with range
	for index, value := range n {
		fmt.Println(index, value)
	}

	// slicing a slice
	// get letters from position 0 to 3 (end is not inclusive)
	name := []string{"A", "B", "C", "D", "E", "F"}
	fmt.Println(name[0:3])

	// get letters from position 1 to the end
	fmt.Println(name[1:])

	// get letters from position 0 to 4 (end is not inclusive)
	fmt.Println(name[:4])

	// append to a slice
	courses := []string{"Finance", "Coding", "English"}
	fmt.Println(courses)
	courses = append(courses, "History", "Arts")
	fmt.Println(courses)

	// append a slice to a slice
	freeCourses := []string{"Music", "Math"}
	courses = append(courses, freeCourses...)

	// delete from slice
	products := []string{"product 1", "product 2", "product 3", "product 4"}
	fmt.Println(products)

	// delete product 2
	products = append(products[:1], products[2:]...)
	fmt.Println(products)

	// use make to define better slice with manageable size
	scores := make([]int, 4, 5)
	fmt.Println(scores)
	fmt.Println(len(scores))
	fmt.Println(cap(scores))

	scores[0] = 1
	scores[1] = 2
	scores[2] = 3
	scores[3] = 4

	// once appended exceeds cap, cap automatically doubles its size
	scores = append(scores, 5, 6, 7, 8)
	fmt.Println(scores)
	fmt.Println(len(scores))
	fmt.Println(cap(scores))

	// multi-dimensional slice
	john := []string{"John", "Mayer"}
	mike := []string{"Mike", "Jane"}
	lila := []string{"Lila", "Gora"}

	employees := [][]string{john, mike, lila}

	fmt.Println(employees)
}
