package main

import "fmt"

func main() {
	var age int = 18
	fmt.Printf("%v (age) is %T.\n", age, age)

	// create a custom type based off of int type
	type entier int

	// we can use it as soon as we have it ready
	var âge entier = 2019
	fmt.Printf("%v (âge) is %T.\n", âge, âge)
}
