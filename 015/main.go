package main

import "fmt"

func main() {
	// By default fallthrough is disabled | once condition met that is it
	switch {
	case false:
		fmt.Println("No print")
	case (2 != 2):
		fmt.Println("No print too")
	case true:
		fmt.Println("Print")
		// this commands to pass to next case even if it is true
		fallthrough
	case (12 > 2):
		fmt.Println("Print too")
		fallthrough
	case (22 == 10):
		fmt.Println("Fallthrough brought me here!!!")
	default:
		fmt.Println("I am the backup!!!")
	}

	// with expression
	switch "go" {
	case "c++":
		fmt.Println("Only C++")
	case "ruby":
		fmt.Println("Ruby is my friend")
	case "go", "elixir", "kotlin":
		fmt.Println("New ones...")
	}
}
