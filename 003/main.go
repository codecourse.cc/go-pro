package main

import "fmt"

// not a good idea but can be useful in some cases
var alien string = "I am not from around..."

func main() {
	// declared but not initialized or assigned a value
	var name string
	fmt.Println("var with string type gets assigned empty string if no assignment given: ", name)

	// declare & assign a value
	var age = 32
	fmt.Println("var is declared and assigned a value (initialized): ", age)

	// declare with type & assign a value
	var score int = 20
	fmt.Println("var is declared with int type and assigned a value: ", score)

	fmt.Println("var can be outside of a func body: ", alien)
}
