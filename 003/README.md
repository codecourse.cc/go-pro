# Use for var
The var keyword is used to declare a variable.

A variable can be declared and assigned a value, which is generally called initialization.

We can also have the var type added when creating a variable.

In Go, when we declare a var with type and not assign a value, a default value (depends on the type of var) will be added automatically.

We are allowed to declare a var outside of a function body but generally it is a bad practice and can pollute the global space. 