package main

import (
	"fmt"
)

func main() {
	// unsigned int | 0 and up
	var u uint = 18446744073709551615
	var u8 uint8 = 255
	var u16 uint16 = 65535
	var u32 uint32 = 4294967295
	var u64 uint64 = 18446744073709551615

	fmt.Printf("%T %T %T %T %T\n", u, u8, u16, u32, u64)
	fmt.Println(u, u8, u16, u32, u64)

	// signed int | negative & positive
	var in int = -9223372036854775808
	var in8 int8 = -128
	var in16 int16 = -32768
	var in32 int32 = -2147483648
	var in64 int64 = -922337203685477580
	var i int = 9223372036854775807
	var i8 int8 = 127
	var i16 int16 = 32767
	var i32 int32 = 2147483647
	var i64 int64 = 9223372036854775807

	fmt.Printf("%T %T %T %T %T\n", in, in8, in16, in32, in64)
	fmt.Println(in, in8, in16, in32, in64)

	fmt.Printf("%T %T %T %T %T\n", i, i8, i16, i32, i64)
	fmt.Println(in, i8, i16, i32, i64)

	// floats
	var f32 float32 = 32.32
	var f64 float32 = 64.64

	fmt.Printf("%T %T\n", f32, f64)
	fmt.Println(f32, f64)

	// complex numbers
	var c64 complex64 = 32.32
	var c128 complex128 = 64.64

	fmt.Printf("%T %T\n", c64, c128)
	fmt.Println(c64, c128)

	// alias
	var b byte = 255
	var r rune = 2147483647

	fmt.Printf("%T %T\n", b, r)
	fmt.Println(b, r)
}
