package main

import "fmt"

func main() {
	if true {
		fmt.Println("Always prints out.")
	}

	if false {
		fmt.Println("Never prints out.")
	}

	// flip
	if !true {
		fmt.Println("Never prints out.")
	}

	// flip
	if !false {
		fmt.Println("Always prints out.")
	}

	// initialization statement | n scoped to if statement only
	if n := 10; n <= 20 {
		fmt.Println(n)
	}

	// this does not have access to n
	// fmt.Println(n)

	// if and else
	if n := 24; n < 12 {
		fmt.Println("It will not be shown!")
	} else {
		fmt.Println("It will be shown!")
	}

	// if, else if and else
	if n := 8; n > 12 {
		fmt.Println("No!")
	} else if n == 8 {
		fmt.Println("Yes!")
	} else {
		fmt.Println("It will be shown if above ones are false")
	}
}
