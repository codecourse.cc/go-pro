package main

import "fmt"

func main() {
	name := "Azar"
	fmt.Printf("%v (name) is a type of %T.\n", name, name)

	age := 18
	fmt.Printf("%v (age) is a type of %T.\n", age, age)

	alive := true
	fmt.Printf("%v (alive) is a type of %T.\n", alive, alive)
}
