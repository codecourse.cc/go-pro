# Type (Basic)

It is all about the type.

Here we only check **Basic** data types:

- Number
- String
- Boolean